﻿using System;
using HomeWork5;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HomeWork5Test
{
    [TestClass]
    public class MyValidatorTest
    {
        [TestMethod]
        public void ValidationIsLetter_Ivan6_False()
        {
            // arrange
            string name = "Ivan6";
            bool expected = false;

            //act
            bool actualResult = MyValidator.ValidationIsLetter(name);

            //assert

            Assert.AreEqual(expected, actualResult);
        }

        [TestMethod]
        public void ValidationFromBaseLib_Range_200and400_True()
        {
            int element = 200;
            int length = 400;
            bool expected = true;

            bool actualResult = MyValidator.ValidationFromBaseLib_Range(element, length);

            Assert.AreEqual(expected, actualResult);
        }
    }
}
