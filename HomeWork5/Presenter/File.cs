﻿using System;
using System.IO;
using System.Windows.Forms;

namespace HomeWork5
{
    class File
    {
        Form1 view;
        SaveFileDialog fileDialog;
        public File(Form1 view, SaveFileDialog fileDialog)
        {
            this.view = view;
            this.fileDialog = fileDialog;
            this.view.saveFile += View_saveFile;
        }

        private void View_saveFile(object sender, EventArgs e)
        {
            if (this.fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Settings settingsFile = Settings.setSettings(this.fileDialog.FileName);
                StreamWriter sw = new StreamWriter(settingsFile.path);
                
                using (sw)
                {
                    sw.Write(SerrializeInCsv.Serrialize());
                }

            }
            
        }
    }
}
