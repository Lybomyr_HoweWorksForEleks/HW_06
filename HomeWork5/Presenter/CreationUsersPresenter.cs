﻿using System;
using System.Windows.Forms;
/// <summary>
/// here write test!!!!!!!
/// </summary>
namespace HomeWork5
{
    class CreationUsersPresenter
    {
        Form1 view;
        CreationUsers creationUsersModel;
        TextBox first_name;
        TextBox last_name;
        TextBox age;
        TextBox work;
        public string result;

        public CreationUsersPresenter(Form1 view, TextBox first_name, TextBox last_name, 
            TextBox age, TextBox work)
        {
            this.view = view;
            this.first_name = first_name;
            this.last_name = last_name;
            this.age = age;
            this.work = work;
            view.creationUser += View_creationUser;
        }

        private void View_creationUser(object sender, EventArgs e)
        {
            if (MyValidator.ValidationIsLetter(first_name.Text) && 
                MyValidator.ValidationIsLetter(last_name.Text) &&
                MyValidator.ValidationFromBaseLib_Range(Convert.ToInt32(age.Text), 100) && 
                MyValidator.ValidationromBaseLib_Length(first_name.Text, 100)
                )
            { 
                creationUsersModel = new CreationUsers(
                    this.first_name.Text, 
                    this.last_name.Text,
                    Convert.ToInt32(this.age.Text), 
                    this.work.Text);
                result = creationUsersModel.ToString();
            }
        }
    }
}
