﻿using System;
using System.Collections.Generic;

namespace HomeWork5
{
    class CreationUsers
    {
        public static readonly List<User> users = new List<User>();
        private User newUser;

        public CreationUsers(string first_name, string last_name, int age, string work)
        {
            newUser = new User(first_name, last_name, age, work);
            users.Add(newUser);
        }

        public override string ToString()
        {
            return $"{DateTime.Now.ToShortTimeString()}" + " " + "Added user - " + $"{newUser.FirstName}";
        }
    }
}
