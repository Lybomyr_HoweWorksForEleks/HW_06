﻿namespace HomeWork5
{
    class Settings
    {
        private static Settings settingPath;
        public string path;

        private Settings(string path)
        {
            this.path = path;
        }

        public static Settings setSettings(string path = "")
        {
            if (settingPath == null)
            {
                settingPath = new Settings(path);
            }
            return settingPath;
        }
    }
}
